// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA1uwknpmSRIwGoTi0ez9dec0cvy57xmes",
    authDomain: "lembrey-4033b.firebaseapp.com",
    databaseURL: "https://lembrey-4033b.firebaseio.com",
    projectId: "lembrey-4033b",
    storageBucket: "",
    messagingSenderId: "832289352584",
    appId: "1:832289352584:web:f0f307beb348e6fa"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
